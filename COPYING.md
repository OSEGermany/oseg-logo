The "Verein"/association parts (the text parts containing the string "e.V.")
of the logo are under the [CC-BY-SA](LICENSE.CC-BY-SA.md).

All the other parts are under the [CC0](LICENSE.CC0.md) license.
