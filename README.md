<!--
SPDX-FileCopyrightText: 2020-2023 Robin Vobruba <hoijui.quaero@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->

# Logos for OSEG (community) and OSEG e.V. (non-profit association)

[![pipeline status](
    https://gitlab.com/OSEGermany/oseg-logo/badges/master/pipeline.svg)](
    https://gitlab.com/OSEGermany/oseg-logo/commits/master)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/oseg-logo)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/oseg-logo)

You may want to consult [the Licensing terms](COPYING.md) for these Logos.

## How does this work

There is one master SVG - the actual source -
which is hand crafted, and stored in this repository.

The actual logos images to use,
are all generated from this one source file.

## Notes for Logo Development

You should never use the above mentioned source SVG,
except when you want to change any of the logos.

It is very strongly recommended to use [Inkscape](
https://inkscape.org/) for editing.

Before committing changes to the SVG,
please apply the SVG beautifier on it.
We currently use [gsvg](https://github.com/hoijui/gsvg) for that task.
If you forget to do that,
your commits will have to be rewritten with the filter applied.

## Where are the logos

The generated logos, which are the ones you should use, can be found here:

<https://osegermany.gitlab.io/oseg-logo/>
