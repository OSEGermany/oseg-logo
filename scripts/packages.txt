# SPDX-FileCopyrightText: 2020-2023 Robin Vobruba <hoijui.quaero@gmail.com>
# SPDX-License-Identifier: Unlicense

git
inkscape
pngquant
pstoedit
tidy
xmlstarlet
zip
python3
python3-pip
