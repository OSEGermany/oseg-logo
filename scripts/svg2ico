#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2020-2023 Robin Vobruba <hoijui.quaero@gmail.com>
# SPDX-License-Identifier: Unlicense
#
# See the output of "$0 -h" for details.

# Exit immediately on each error and unset variable;
# see: https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -Eeuo pipefail
#set -Eeu

# Dependencies:
# apt-get install pngquant inkscape

script_path="$(readlink -f "${BASH_SOURCE[0]}")"
script_dir="$(dirname "$script_path")"
script_name="$(basename "$script_path")"
root="$script_dir/.."
target="$root/target"
size=(16 32 24 48 72 96 144 152 192 196)
ico="$target/favicon.ico"

function print_help() {

	echo -e "$script_name -"
	echo -e "Converts one SVG into a multi-size ICO (Icon) file."
	echo -e ""
	echo -e "Usage:"
	echo -e "\t$script_name [OPTION...] <SVG-FILE>"
	echo -e "Options:"
	echo -e "\t-h, --help"
	echo -e "\t\tPrint this usage help and exits"
	echo -e ""
}

# Process command line arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
	arg="$1"
	shift # $2 -> $1, $3 -> $2, ...

	case "$arg" in
		-h|--help)
			print_help
			exit 0
			;;
		*) # non-/unknown option
			POSITIONAL+=("$arg") # save it in an array for later
			;;
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

svg="$1"

echo
echo "'$(basename "$svg")' -> '$(basename "$ico")' ..."

echo "  - Making bitmaps from '$(basename "$svg")' ..."
for sz in "${size[@]}"
do
	# inkscape version < 1.0
	#inkscape "$svg" --export-png="$svg-favicon-$sz.png" -w $sz -h $sz --without-gui
	# inkscape version >= 1.0
	inkscape "$svg" --export-filename="$svg-favicon-$sz.png" -w "$sz" -h "$sz" >/dev/null 2>&1
done

echo "  - Compressing the bitmaps ..."
## Replace with your favorite (e.g. pngquant)
# optipng -o7 favicon-*.png
pngquant -f --ext .png "$svg-favicon-"*".png" --posterize 4 --speed 1 >/dev/null 2>&1

echo "  - Combining the bitmaps into '$(basename "$ico")' ..."
convert $(ls -v "$svg-favicon-"*".png") "$ico"

## Clean-up
rm "$svg-favicon-"*".png"

echo "done."
